// Display main contaier and modal only when the entry animation ends
$(document).ready(function () {
  let $container = $('.container');
  let waitingForEndAnimation = setInterval(() => {
    if (mainAimationFinished == true) {
      $container.fadeIn('slow', () => {
        $(".modal").modal("show");
      });
      clearInterval(waitingForEndAnimation);
    }
  }, 1000);
});