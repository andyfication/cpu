// Check when the intro animation ends
var mainAimationFinished = false;

$(document).ready(function () {
  let $introContainer = $('#introContainer');
  let $beginButton = $('#begin');
  let $introImageActive = $('#introImageActive');
  // Intro CPU image display 
  setTimeout(function () {
    $introContainer.fadeIn(4000);
  }, 1000);
  // Swap images animation 
  $beginButton.on('click', function () {
    $introImageActive.fadeIn(3000, function () {
      $introContainer.fadeOut(2000, function () {
        mainAimationFinished = true;
      });
    });
  });
});